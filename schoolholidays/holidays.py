import os

import pandas as pd

from schoolholidays.config import DATASETS_ABS_PATH


def list_available_holidays_datasets():
    return [f for f in os.listdir(DATASETS_ABS_PATH) if f.endswith(".csv")]


def load_holidays_datasets(datasets=None):
    if datasets is None:
        datasets = list_available_holidays_datasets()
    full_df = None
    for dataset in datasets:
        name = dataset.split(".")[0]
        path = os.path.join(DATASETS_ABS_PATH, dataset)
        df = pd.read_csv(path, parse_dates=["date"])
        df = df.add_prefix("%s_" % name)
        df.columns = ["date"] + df.columns[1:].tolist()
        if full_df is None:
            full_df = df
        else:
            full_df = pd.merge(full_df, df, on="date", how="outer")
    return full_df


def merge_holidays_datasets(df: pd.DataFrame, datasets=None, date_column_name="date"):
    df_holidays = load_holidays_datasets(datasets)
    df_merged = pd.merge(df, df_holidays, how="left", left_on=date_column_name, right_on="date")
    if date_column_name != "date":
        df_merged = df_merged.drop(columns="date")
    return df_merged
